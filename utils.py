from pathlib import Path
from typing import Any, Dict, List, Tuple, Union

import torch
from torch import Tensor
from torch.nn import Module
from tvm import auto_scheduler as ansor
from tvm import relay
from tvm.auto_scheduler.search_task import HardwareParams
from tvm.target import Target

PathLike = Union[Path, str]


def load_tuned_configs(
    dnn: Module, example_input: Tensor, json_file: str, target: Target
):
    mod, params = gen_tvm_model(dnn, example_input)
    hw_params = HardwareParams(
        num_cores=-1,
        vector_unit_bytes=16,
        cache_line_bytes=64,
        max_shared_memory_per_block=49152,
        max_local_memory_per_block=2147483647,
        max_threads_per_block=1024,
        max_vthread_extent=8,
        warp_size=32,
    )
    tasks, task_weights = ansor.extract_tasks(
        mod["main"], params, target, hardware_params=hw_params
    )
    tuner = ansor.TaskScheduler(tasks, task_weights, load_log_file=json_file)
    return _load_tuned_configs(tuner)


def _load_tuned_configs(tuner: ansor.TaskScheduler):
    from collections import defaultdict

    from tvm.auto_scheduler import RecordReader, SearchTask
    from tvm.auto_scheduler.feature import (
        get_per_store_features_from_measure_pairs as get_feats,
    )

    records_by_task = defaultdict(list)
    tuner_tasks = {task.workload_key: task for task in tuner.tasks}
    for inp, res in RecordReader(tuner.load_log_file):
        if res.error_no == 0:
            task = tuner_tasks[inp.task.workload_key]
            records_by_task[task].append((inp, res))
    ret: Dict[SearchTask, List[Dict[str, Any]]] = {}
    for task, records in records_by_task.items():
        # `inputs[i].task` is not initialized when read from JSON file
        # -- we use tuner_tasks instead
        records = [
            (input_, result) for input_, result in records if result.error_no == 0
        ]
        inputs, results = zip(*records)
        features, norm_throughputs, _ = get_feats(inputs, results)
        flops = task.compute_dag.flop_ct
        task_config = []
        for i in range(len(inputs)):
            costs = results[i].costs
            # mean_cost is FloatImm type before conversion
            mean_cost = float(sum(costs) / len(costs))
            task_config.append(
                {
                    "state": inputs[i].state,
                    "features": features[i].tolist(),
                    "throughput": norm_throughputs[i],
                    "time": mean_cost,
                    "flops": flops,
                }
            )
        ret[task] = task_config
    return ret


def gen_tvm_model(model: Module, input_tensor: torch.Tensor, to_nhwc: bool = True):
    scripted_model = torch.jit.trace(model, input_tensor)

    shape_list = [("input", input_tensor.shape)]
    mod, params = relay.frontend.from_pytorch(scripted_model, shape_list)
    if to_nhwc:
        mod = _convert_to_nhwc(model, mod)
    return mod, params


def _convert_to_nhwc(model: Module, mod):
    from torch.nn import Conv2d
    from torch.nn.intrinsic.quantized import ConvReLU2d as QConvReLU2d
    from torch.nn.quantized import Conv2d as QConv2d
    from tvm import transform

    for module in model.modules():
        if not isinstance(module, (Conv2d, QConv2d, QConvReLU2d)):
            continue
        cin = module.in_channels
        groups = module.groups
        if groups != 1 and cin != groups:
            return mod
    # Convert the layout to NHWC
    # RemoveUnunsedFunctions is used to clean up the graph.
    converter = relay.transform.ConvertLayout(
        {
            "nn.conv2d": ["NHWC", "default"],
            "nn.max_pool2d": ["NHWC", "default"],
            "qnn.conv2d": ["NHWC", "default"],
        },
    )
    seq = transform.Sequential([relay.transform.RemoveUnusedFunctions(), converter])
    with transform.PassContext(opt_level=3):
        mod = seq(mod)
    return mod


def parse_task(task):
    import ast

    # Workload key is a list [hash, *in_shapes, out_shape]; we remove the hash from the list
    key: list = ast.literal_eval(task.workload_key)
    in_shapes, out_shape = key[1:-1], key[-1]
    return _parse_tvm_func_name(task.desc, in_shapes, out_shape)


def _parse_tvm_func_name(
    name: str, in_shapes: list, out_shape
) -> Tuple[List[Tuple[str, int]], str]:
    INPLACES = {
        "add": "+",
        "subtract": "-",
        "cast": "Cast",
        "clip": "Clip",
        "divide": "Div",
        "nn_max": "Max",
        "multiply": "Mul",
        "nn_relu": "ReLU",
        "sigmoid": "Sigmoid",
    }

    CORES = {
        "nn_adaptive_avg_pool2d": ("AdaAvgPool", 1),
        "nn_avg_pool2d": ("AvgPool", 1),
        "nn_max_pool2d": ("MaxPool", 1),
        "nn_contrib_conv2d_NCHWc": ("ConvNCHWc", 2),
        "nn_contrib_conv2d_winograd_without_weight_transform": ("ConvW", 2),
        "nn_conv2d": ("Conv", 2),
        "nn_dense": ("Dense", 2),
        "mean": ("RedMean", 1),
    }

    def parse_typename(
        typename: str, in_shapes: list, out_shape, is_int: bool = False
    ) -> Tuple[str, str]:
        if typename.endswith("_fixed_point"):
            typename, shape = parse_typename(typename[:-12], in_shapes, out_shape, True)
            if not is_int:
                return f"Int({typename})", shape
            return typename, shape
        for k, v in INPLACES.items():
            suffix = f"_{k}"
            if not typename.endswith(suffix):
                continue
            typename, shape = parse_typename(
                typename[: -len(suffix)], in_shapes, out_shape, is_int
            )
            typename = f"{typename}/{v}" if typename else f"{v}"
            return typename, shape
        if typename == "vm_mod_fused":
            # A chain of inplaces without core operation.
            return "", f"{in_shapes[0]} -> {out_shape}"
        for k, (to_name, nargs) in CORES.items():
            suffix = f"_{k}"
            if not typename.endswith(suffix):
                continue
            assert len(in_shapes) >= nargs
            in_shape_desc = " x ".join(str(shape) for shape in in_shapes[:nargs])
            return to_name, f"{in_shape_desc} -> {out_shape}"
        return typename, "?"

    def parse_name(name: str, in_shapes: list, out_shape):
        try:
            typename, idx = name.rsplit("_", 1)
            idx = int(idx)
        except ValueError:
            typename = name
            idx = 0
        typename, shape_desc = parse_typename(typename, in_shapes, out_shape)
        return typename, idx, shape_desc

    segments = name.split(",")
    types_indices = []
    shapes = set()
    for s in segments:
        type_, indice, shape = parse_name(s, in_shapes, out_shape)
        types_indices.append((type_, indice))
        shapes.add(shape)
    assert len(shapes) == 1
    return types_indices, list(shapes)[0]


def short_print_names(types_indices: List[Tuple[str, int]]):
    if len(types_indices) == 1:
        ty, indice = types_indices[0]
        return f"{ty}[{indice}]"
    types, indices = zip(*types_indices)
    types = set(types)
    if len(types) == 1:
        ty = types.pop()
        return f"{ty}{sorted(indices)}"
    return ",".join(f"{ty}[{indice}]" for ty, indice in types_indices)

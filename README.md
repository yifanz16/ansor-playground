# Experimenting with TVM Performance Model

- This is a tutorial/exercise for getting started with the compiler orchestrator project.

As mentioned in the project description:

> Performance optimizers for deep learning models are a major part of these tools,
> but we also include tools for other goals in other domains.

We're looking at one such performance optimization tool for DNNs
(specifically, performance here refers to latency/throughput of DNN inference).

[TVM](https://tvm.apache.org/) is a state-of-the-art machine learning compiler framework.
Put briefly, it's capable of generating machine code for DNN inference
onto various hardware of different architecture and computing power,
such that the performance of generated code is near-optimal on each hardware.
To do so, TVM _searches_ for a chain of code transformation steps (a _configuration_)
that turns the initial ("naive") code into one that suits the given hardware.

In this search process, TVM needs to evaluate each configuration for its fitness;
this involves running the generated code on hardware, which can be slow.
Instead, TVM has a _prediction model_ to predict the performance of a configuration.
This model is based on XGBoost, an improved kind of [decision forest](https://en.wikipedia.org/wiki/Random_forest).

The goal is for you to get familiar with this prediction model contained in TVM;
we will inspect how well this and other possible choices of models (such as LSTM, CNN, etc.)
work on various workloads (DNNs).

## Environment Setup

We'll use a few python packages in this project that require some setup.
The use of a virtual-env management tool, such as `conda` is highly encouraged.
- You can get familiar with conda
  from [here](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/)
  or [here](https://docs.conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf).

To install the Python environment for TVM:

1. Create a virtual environment and activate it, if you're using conda.
   - When creating the virtual environment we recommend `python=3.8`.

     If you're using conda, `conda create -n tvm python=3.8` will do.

2. Install TVM directly: `pip install tvm`.

3. Install a few more packages: `pip install numpy torch torchvision matplotlib`

## Tasks

- Code template is provided as `main.py`.
  Instead of writing from scratch, you can modify what is written there for the following tasks.

- If you feel stuck or uncertain, you're very welcome to ask questions and discuss with me in the email.

1. Check out a short [walkthrough on how to use Ansor](https://tvm.apache.org/docs/tutorial/auto_scheduler_matmul_x86.html)
   in the documentation, to get a sense of TVM's workflow.
   Ansor (or Autoscheduler) is the name of the _new_ tool for the code search mentioned above in TVM.
   AutoTVM is its older counterpart that we're not interested in.

   Then, search for `XGBModel` in the [documentation](https://tvm.apache.org/docs/index.html)
   and read as much as you feel necessary, but feel free to skip the details.
   This should explain how the performance prediction model works.

1. In `main.py`, create a ResNet-50 or VGG19 as the DNN that TVM will optimize;
   then, using the documentation on `XGBModel`,
   load XGBModel from either `./resnet50.xgboost` (if ResNet-50) or `./vgg19.xgboost` (if VGG19).
   Both files are attached within the repo.
   Finally, if you're using VGG19, replace "./resnet50_configs.json" with "./vgg19_configs.json" in `utils.load_tuned_configs()`.

1. Within the for loop, play with variables `task` and `configs` to understand what we have read from the `*.json`
   configs file.

   You may want to scan at this [documentation](https://tvm.apache.org/docs/how_to/tune_with_autoscheduler/tune_network_cuda.html)
   to see how TVM

   > partition the network into small subgraphs [tasks] and tune them independently.

   This will explain what the `task` is.

1. The code for invoking the prediction model is provided, and `predicted_perf` holds the predicted performance.
   Compare and contrast that against `groundtruth_perf` (measured performance on hardware)
   to see how well the predictive model is doing.
   It's suggested that you plot the predicted against the groundtruth (in an effective way) for each task in the network.

1. Now, try different pairings between the XGB model and the DNN: `vgg19.xgboost` with `resnet50()`,
   `resnet50.xgboost` with `vgg19()`, etc.
   Repeat the step above and draw a conclusion on how good the predictions are under these pairing.

1. [**] Have a look at the so-called "features" inputs to the XGB model.
   These features are computed on line 43 of `./utils.py`,
   which ultimately comes from inside of TVM.

   [This](https://github.com/apache/tvm/blob/main/src/auto_scheduler/feature.cc#L1252) function in TVM
   shows the name of these features (from which you may guess the meaning).
   These features are extracted from the _generated code_ (what you get after applying the transformation steps in the configuration),
   not the configuration itself.

   Should a prediction model trained on the layers in one network work on layers in another network?
   Did that work out according to the results you get in the previous step?

## Deliverable

- Send as a tarball in the email, the `main.py` file with your code filled in,
  as well as the figures you generated for the tasks above.

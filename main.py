import torch
from tvm.auto_scheduler.cost_model import xgb_model as ansor_xgb
import numpy as np
from matplotlib import pyplot as plt
from tvm.target import cuda

import utils


def main():
    # TODO: replace this with your DNN to be optimized
    from torchvision.models import resnet50, vgg19_bn
    dnn = None

    # TODO: replace this with your loaded prediction model
    from tvm.auto_scheduler import XGBModel
    bst_model = None

    target = cuda(model="2080ti", arch="sm_75")
    configs_by_task = utils.load_tuned_configs(
        dnn,
        torch.rand(1, 3, 224, 224),
        "./resnet50_configs.json",  # TODO: or "./vgg19_configs.json"
        target,
    )
    for task, configs in configs_by_task.items():
        # TODO: play with `task` and `configs` here.
        names, shape_desc = utils.parse_task(task)
        name = utils.short_print_names(names)
        print(f"{name}: {shape_desc}")
        groundtruth_perf = [c["throughput"] for c in configs]
        features = [np.array(c["features"]) for c in configs]
        features = np.array(features, dtype=object)
        # features: [batch_size, n_buf, n_features]
        dtest, pack_ids = ansor_xgb.feature_to_pack_sum_xgbmatrix(features)
        # TODO: run model prediction here.
        raw_preds = None   # HINT: bst_model.?.?(dtest)
        predicted_perf = ansor_xgb.predict_throughput_pack_sum(raw_preds, pack_ids)
        # predicted: [batch_size]
        # TODO: compare predicted_perf and groundtruth_perf


if __name__ == "__main__":
    main()
